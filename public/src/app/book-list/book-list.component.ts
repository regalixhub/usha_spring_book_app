import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { Book } from '../model/book';
import { BooksService } from '../services/books.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html'
})
export class BookListComponent implements OnInit {

  public books: Book[];

  constructor(private router: Router,
    private aroute: ActivatedRoute,
    private booksService: BooksService) { }

  ngOnInit() {
    this.booksService.getBooks().subscribe(data => {
      this.books = data;
      this.books.sort(function(a, b) {
        return a.bookId - b.bookId;
      });
    });
  }

  updateBooks(id: number) {
    this.router.navigate(['/bookUpdate', id]);
  }

  bookDetail(id: number) {
    this.router.navigate(['/bookDetails', id]);
  }

  delete(id: number, i: number) {
    this.booksService.deleteBook(id).subscribe();
    this.books.splice(i, 1);
  }

}
