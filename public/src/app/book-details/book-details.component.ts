import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

import { Book } from '../model/book';
import { BooksService } from '../services/books.service';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html'
})
export class BookDetailsComponent implements OnInit {

  public book: Book;

  constructor(private aroute: ActivatedRoute,
    private router: Router,
    private booksService: BooksService) { }

  ngOnInit() {
    this.aroute.paramMap.subscribe((params: ParamMap) => {
      const id = parseInt(params.get('bookId'), 10);
      this.booksService.getBookById(id)
      .subscribe(data => this.book = data);
    });
  }

}
