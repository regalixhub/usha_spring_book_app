package io.regalix.springboot.rest;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.regalix.springboot.exception.ResourceNotFoundException;
import io.regalix.springboot.model.Book;
import io.regalix.springboot.repository.BookRepository;

@RestController
@RequestMapping("api/book")
@CrossOrigin(origins= {"http://localhost:4200"})
public class BookRestController {

	@Autowired
	private BookRepository bookRepo;
	
	@GetMapping("/")
	public List<Book> getAllBook() {
		return bookRepo.findAll();
	}
	
	@GetMapping("/{bookId}")
	public Optional<Book> getOneBookById(@PathVariable Long bookId) {
		return bookRepo.findById(bookId);
	}
	@PostMapping("/")
	public Book addBook(@Valid @RequestBody Book book) {
		return bookRepo.save(book);
	}
	
	@PutMapping("/{bookId}")
	public Book updateBook(@PathVariable Long bookId, @Valid @RequestBody Book book) {
		Book bookEdit = bookRepo.getOne(bookId);
		bookEdit.setIsbn(book.getIsbn());
		bookEdit.setBookTitle(book.getBookTitle());
		bookEdit.setBookAuthor(book.getBookAuthor());
		bookEdit.setBookDescription(book.getBookDescription());
		Book bookEdited = bookRepo.save(bookEdit);
		return bookEdited;
	}
	
	@DeleteMapping("/{bookId}")
    public ResponseEntity<?> deleteBook(@PathVariable Long bookId) {
    	Book _task = bookRepo.findById(bookId)
    			.orElseThrow(() -> new ResourceNotFoundException("Book", "id", bookId));
    	bookRepo.delete(_task);
    	
    	return ResponseEntity.ok().build();
    }
	
}
