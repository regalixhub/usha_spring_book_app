package io.regalix.springboot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import io.regalix.springboot.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

}
